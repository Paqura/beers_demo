export const filterListSelector = (state, value) => {
  return state.beers.filter(it => it.name.toLowerCase().includes(value));
}

export const addToFavoriteSelector = (state, id) => {
  return state.favorites.concat(state.beers.filter(it => it.id === id));
}