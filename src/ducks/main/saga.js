import { put, take, all} from 'redux-saga/effects';
import {
  FETCH_BEERS_REQUEST, 
  FETCH_BEERS_SUCCESS, 
  FETCH_BEERS_FAILURE, 
  ADD_TO_FAVORITE_REQUEST, 
  ADD_TO_FAVORITE_SUCCESS
} from './types';
import axios from 'axios';
import config from '../../config';

export const fetchBeersSage = function* () {
  while(true) {
    yield take(FETCH_BEERS_REQUEST);
    
    try {
      const ref = yield axios(config.apiURI);
      yield put({
        type: FETCH_BEERS_SUCCESS,
        payload: ref.data
      })
    } catch(err) {
      yield put({
        type: FETCH_BEERS_FAILURE,
        payload: err
      })
    }   
  }
};

export const addToFavoriteSaga = function* () {
  while(true) {
    const action = yield take(ADD_TO_FAVORITE_REQUEST);

    if(action.payload) {
      yield put({
        type: ADD_TO_FAVORITE_SUCCESS,
        payload: action.payload
      })
    } else {
      yield put({
        type: FETCH_BEERS_FAILURE,
        payload: 'Нет такого id'
      })
    }
  }
};

export default function* () {
  yield all([
    fetchBeersSage(),
    addToFavoriteSaga()
  ])
}