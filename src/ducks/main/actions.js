import {FETCH_BEERS_REQUEST, FILTER_BEERS, ADD_TO_FAVORITE_REQUEST} from './types';

export const fetchBeers = () => {
  return {
    type: FETCH_BEERS_REQUEST
  }  
}

export const filterBeersListAction = (value) => {
  return {
    type: FILTER_BEERS,
    payload: value
  } 
}

export const addToFavoriteAction = (id) => {
  return {
    type: ADD_TO_FAVORITE_REQUEST,
    payload: id
  }
}