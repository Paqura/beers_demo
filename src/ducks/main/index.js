import {
  FETCH_BEERS_REQUEST, 
  FETCH_BEERS_SUCCESS, 
  FETCH_BEERS_FAILURE,
  FILTER_BEERS,
  ADD_TO_FAVORITE_REQUEST,
  ADD_TO_FAVORITE_SUCCESS
} from './types';

import {filterListSelector, addToFavoriteSelector} from './selectors';

const initialState = {
  beers: null,
  loading: false,
  filtered: null,
  favorites: []
};

export default function reducer(state = initialState, action) {
  const { type, payload } = action;

  switch(type) {
    case FETCH_BEERS_REQUEST:
      return {
        ...state,
        loading: true
      }
    case FETCH_BEERS_SUCCESS:
      return {
        ...state,
        loading: false,
        beers: payload
      }  
    case FETCH_BEERS_FAILURE: 
      return {
        ...state,
        loading: false,
        beers: null
      }  

    case FILTER_BEERS: 
      const filteredList = filterListSelector(state, payload);
      return {
        ...state,
        filtered: filteredList
      }  

    case ADD_TO_FAVORITE_REQUEST:
      return {
        ...state,
        loading: true
      }
    case ADD_TO_FAVORITE_SUCCESS:
      const newFavorites = addToFavoriteSelector(state, payload);
      return {
        ...state,
        loading: false,
        favorites: newFavorites
      }    
    default: 
      return state;
  }
}