import React from 'react';
import { Link } from 'react-router-dom';
import logo from './logo.svg';

export default () => {
  return (
    <Link to="/" className="logotype">
      <img src={logo} width="40" height="40" alt="Логотип"/>
    </Link>
  )
}
