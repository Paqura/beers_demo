import React from 'react'
import { NavLink } from 'react-router-dom';
import styled from 'styled-components';

export const NavList = styled.ul`
  list-style-type: none;
  padding: 0;
  margin: 0;
  display: flex;
  align-items: center;
`;

export const Counter = styled.span`
  display: inline-block;
  margin-left: 8px;
  font-weight: bold;
  border-radius: 50%;
`;

const NavLinkStyle = {
  textDecoration: 'none',
  marginLeft: '16px',
  fontSize: '14px',
  textTransform: 'uppercase',
  letterSpacing: '1px',
  color: '#37474F'
};

export default ({ counter }) => {
  return (
    <NavList className="navigation__list">
      <li className="navigation__item">
        <NavLink to="/" className="navigation__link" style={NavLinkStyle}>Главная</NavLink>
      </li>
      <li className="navigation__item">
        <NavLink to="/favorites" className="navigation__link" style={NavLinkStyle}>
          Избранное
          <Counter>{counter}</Counter>
        </NavLink>
      </li>
    </NavList>
  )
}
