import React, {Component} from 'react';
import Logo from './logo';
import List from './list';
import styled from 'styled-components';
import { connect } from 'react-redux';

export const NavHeader = styled.header`
  box-shadow: 2px 4px 10px rgba(0,0,0,.2);
`;

export const Navigate = styled.nav `
  display: flex;
  justify-content: space-between;
  max-width: 1140px;
  margin: 0 auto;
  padding: 8px 0;
`;
class Navigation extends Component {
  render() {
    const {favoritesCount} = this.props;
    return (
      <NavHeader>
        <Navigate>
          <Logo/>
          <List counter={favoritesCount}/>
        </Navigate>
      </NavHeader>
    )
  }
}

const mapStateToProps = state => ({
  favoritesCount: state.main.favorites.length
})

export default connect(mapStateToProps, null)(Navigation);
