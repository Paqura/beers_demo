import React from 'react';
import styled from 'styled-components';

export const BeerCard = styled.article`
  display: grid;
  grid-template-columns: 1fr 2fr;
  box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);
  transition: all 0.3s cubic-bezier(.25,.8,.25,1);
  padding: 32px;
  position: relative;
`;

export const ImgWrapper = styled.span`
  display: flex;
  justify-content:center;
  align-items: center;
  width: 60px;
  height: 120px;
`;

export const BeerImg = styled.img`
  max-width: 100%;
  height: 100%;
  object-fit: contain;
  display: block;
`;

const FavoriteButton = styled.button`
  position: absolute;
  right: 8px;
  top: 8px;
  background: none;
  border: 1px solid #ccc;
  font-size: 12px;
  &:focus, &:active {
    outline: none;
  }
  &:hover {
    border: 1px solid red;
  }
`;

const renderDescription = str => {
  if(str.length > 60) {
    str = str.slice(0, 60) + '...';
    return str;
  }
} 

const addToFavorite = (fn, id) => {
  fn(id)
}

const renderBeers = (beer, fn, favorites) => {
  const shortDescription = renderDescription(beer.description);
  const isFavorite = favorites.some(it => it.id === beer.id);
  return (
    <BeerCard key={beer.id}>
      <ImgWrapper>
        <BeerImg src={beer.image_url} alt="Картинка пива"/>
      </ImgWrapper>
      <div className="card-content">
        <h3>{beer.name}</h3>
        <p>{shortDescription}</p>
      </div>

      { fn && 
      <FavoriteButton 
        className={isFavorite ? '--favorite' : ''}  
        onClick={addToFavorite.bind(null, fn, beer.id)}
      >
        Нравится
      </FavoriteButton>  
      }    
    </BeerCard>
  )
}

export default ({beers, addToFavoriteAction, favorites}) => {
  return  beers ? beers.map(it => renderBeers(it, addToFavoriteAction, favorites)) : []
}
