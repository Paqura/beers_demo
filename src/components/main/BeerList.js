import React from 'react';
import BeerItem from './BeerItem';
import styled from 'styled-components';

const BeerList = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr 1fr;
  grid-gap: 16px;
`;

export default ({beers, addToFavoriteAction, favorites}) => {
  return (
    <BeerList>
      <BeerItem 
        beers={beers} 
        addToFavoriteAction={addToFavoriteAction}
        favorites={favorites}
      />
    </BeerList>
  )
}
