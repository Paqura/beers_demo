import React, {Component} from 'react';
import styled from 'styled-components';

export const SearchForm = styled.form`
  margin: 32px 0;
`;

export const SearchInput = styled.input`
  max-width: 600px;
  min-width: 600px;
  padding: 8px;
  border: 0;
  border-bottom: 2px solid #B2EBF2;
  display: flex;
  margin: 0 auto;
  transition: all 400ms ease-out;  
  &[placeholder] {
    font-size: 18px;
  }
  &:focus {
    outline: none;
    border-bottom: 2px solid #00BCD4;
  }
`;



export default class SearchBar extends Component {
  filterBeersList = (evt) => {
    this.props.filterBeersListAction(evt.target.value)
  }
  render() {
    return (
      <SearchForm className='serch-form'>
        <SearchInput type="search" placeholder="Поиск..." onKeyUp={this.filterBeersList}/>
      </SearchForm>
    )
  }  
}
