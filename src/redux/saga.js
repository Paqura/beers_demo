import { all } from 'redux-saga/effects';
import mainPageSaga from '../ducks/main/saga';

export const saga = function * () {
  yield all([
    mainPageSaga()
  ])
};
