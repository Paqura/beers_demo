import { combineReducers } from 'redux';
import { routerReducer as router } from 'react-router-redux';
import mainReducer from '../ducks/main';

export default combineReducers({
  router,
  main: mainReducer
})