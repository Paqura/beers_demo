import React, { Component } from 'react'
import SearchBar from '../components/main/SearchBar';
import BeerList from '../components/main/BeerList';
import { fetchBeers, filterBeersListAction, addToFavoriteAction } from '../ducks/main/actions';
import { connect } from 'react-redux';

class MainPage extends Component {
  componentDidMount() {
    this.props.fetchBeers();
  }
  render() {
    const { beers, filtered, filterBeersListAction, addToFavoriteAction, favorites } = this.props;
    return (
      <div className="container">
        <SearchBar filterBeersListAction={filterBeersListAction} />
        <BeerList 
          beers={filtered && filtered.length ? filtered : beers} 
          addToFavoriteAction={addToFavoriteAction} 
          favorites={favorites} 
        />
      </div>
    )
  }
}

const mapStateToProps = state => ({
  beers: state.main.beers,
  filtered: state.main.filtered,
  favorites: state.main.favorites
});

const mapDispatchToProps = ({
  fetchBeers,
  filterBeersListAction,
  addToFavoriteAction
});

export default connect(mapStateToProps, mapDispatchToProps)(MainPage);
