import React, { Component } from 'react';
import BeerList from '../components/main/BeerList';
import { connect } from 'react-redux';

class FavoritesPage extends Component {
  render() {
    return (
      <div className="container mt-5">
        <BeerList 
          beers={this.props.favorites} 
          addToFavoriteAction={null} 
          favorites={[]} 
        />
      </div>
    )
  }
}

const mapStateToProps = state => ({
  favorites: state.main.favorites
});

export default  connect(mapStateToProps, null)(FavoritesPage);
